`ifndef _ALU //So module is not added multiple times
`define _ALU

module ALU(control, a, b, out);

//Port Definitions
input [2:0] control;
input [31:0] a;
input [31:0] b;

output [31:0] out;

//Declare port datatypes (wire for simple in/out, reg for data storage)
wire[2:0] control;
wire[31:0] a;
wire[31:0] b;
reg[31:0] out;

//Module code

//Whenever control is passed a value this block executes, this is so that a or b passed alone 
//doesn't trigger the ALU
always @(control) begin 
	
	case(control)//Check for one of these cases involving the control input
		3'b000: out <= a & b;
		3'b001: out <= a | b;
		3'b010: out <= a + b;
		//3'b011: NOT USED
		3'b100: out <= a & ~b;
		3'b101: out <= a | ~b;
		3'b110: out <= a - b;
		3'b111: out <= (a < b ? 32'h00000001 : 32'h00000000);
		default: out <= 0;
	endcase
	
end
		
		
endmodule

`endif