`ifndef _fullAdder //So module is not added multiple times
`define _fullAdder

module fullAdder(a, b, cin, sum, cout);

//Port definitions
input a;
input b;
input cin;

output sum;
output cout;

//Declare port datatypes (wire for simple in/out, reg for data storage)
wire a;
wire b;
wire cin;
wire sum;
wire cout;

//Module code
assign {cout,sum} = a + b + cin; //add a + b + cin storing overflow to cout

	
endmodule

`endif