module InstructionMemory(instruction,pc,clk);

	input clk;
	input [31:0] pc; //PC counter is a 32 bit memory address
	output [31:0] instruction; //instruction is the outputed instruction to be run;
	
	reg [31:0] instruction; //Declare instruction as a register becuase value needs to have pursistance between modules

	parameter NumOfInstructions = 10; //Number of instructions to load into memory
	parameter InputFile = "input_instructions.txt"; //Name of input file
	
	reg [7:0] memory [NumOfInstructions:0]; //Create instruction memory based on number of needed instructions(with one terminating blank instruction)
	
	
	initial begin // On initialization only(meaning the code only runs once on startup)
	
	/* THIS CODE IS FOR MANUAL ENTRY WITHOUT UISING TEXT FILE, IF USED COMMENT OUT CODE MARKED BELOW 
	memory[32'd0] = 32'b00000000000000000000000000000000;
	memory[32'd1] = 32'b00000000000000000000000000000001;
	memory[32'd2] = 32'b00000000000000000000000000000010;
	memory[32'd3] = 32'b00000000000000000000000000000011;
	memory[32'd4] = 32'b00000000000000000000000000000100;
	*/

	
	//COMMENT CODE BELOW TO USE MANUAL ENTRY
	
	//All data in text file must be in hexadecimal
	
		$readmemh(InputFile, memory, 0, NumOfInstructions-1); //Reads file into memory
	
	//COMMENT CODE ABOVE TO USE MANUAL ENTRY
	
	
	end
	always @(posedge clk) //Whenever clock is on posedge
		begin
			assign instruction = {memory[pc], memory[pc+1], memory[pc+2], memory[pc+3]}; //Assign instuctution to instruction locatied at memory address pc.
		end
endmodule