`ifndef _control
`define _control

module control(Clk,opcode, funct, regdst, jump, branch, memread, memtoreg, alucontrol, memwrite, alusrc, regwrite);

	//Port Definitions
	input[5:0] opcode;
	input[5:0] funct;
	input Clk;
	output regdst, jump, branch, memread, memtoreg, memwrite, alusrc, regwrite;
	output[2:0] alucontrol;

	//Declare port datatypes (wire for simple in/out, reg for data storage)
	reg regdst, jump, branch, memread, memtoreg, memwrite, alusrc, regwrite;
	reg[2:0] alucontrol;

	always @(posedge Clk) 
	begin //Can change this to run on clk if need be(or on *)
		//Set values to defaults(to minimize changes that have to be made to each control bit on clk)
		regdst		<= 1'b0;
		jump		<= 1'b0;
		branch		<= 1'b0;
		memread		<= 1'b0;
		memtoreg	<= 1'b0;
		memwrite	<= 1'b0;
		alusrc		<= 1'b0;
		regwrite	<= 1'b0;
		alucontrol[2:0]	<= 3'b000;
		
		case(opcode)
			6'b000000: 
				begin //R type
				regwrite	<= 1'b1;
				regdst		<= 1'b1;
				case(funct)
					6'b100000: begin //Add
						alucontrol	<= 3'b010;
						end
					6'b100010: begin //Subtract
						alucontrol	<= 3'b110;
						end
					6'b100100: begin //and
						alucontrol	<= 3'b000;
						end
					6'b100101: begin //or
						alucontrol	<= 3'b001;
						end
					6'b101010: begin //slt
						alucontrol	<= 3'b111;
						end
				endcase
				end
			
			6'b100011: begin //Load word
				regwrite	<= 1'b1;
				alusrc		<= 1'b1;
				memtoreg	<= 1'b1;
				alucontrol	<= 3'b010; //ALU op is 00 so add
				end
			
			6'b101011: begin //Store word
				alusrc		<= 1'b1;
				memwrite	<= 1'b1;
				alucontrol	<= 3'b010; //ALU op is 00 so add
				end
				
			6'b000100: begin //beq
				branch		<= 1'b1;
				alucontrol	<= 3'b110; //ALU op is 01 so subtract
				end
				
			6'b001000: begin //addi
				regwrite	<= 1'b1;
				alusrc		<= 1'b1;
				alucontrol	<= 3'b010; //ALU op is 00 so add
				end
			
			6'b000010: begin //jump
				jump	<= 1'b1;
				end
				
		endcase
	end
endmodule

`endif	