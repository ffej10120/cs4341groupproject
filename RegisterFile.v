//-------------------------------------
//
//	Register File
//
//-------------------------------------
//	Julian McNichols
//	CS 4341.002
//
//
//-------------------------------------

module RegisterFile(A1, A2, A3, WD3, WE3, RD1, RD2, clk);

	input WE3, clk;
	input [4:0]	A1, A2, A3;
	input [31:0] WD3;
	output [31:0]RD1, RD2;
	
	wire [31:0]	RD1, RD2;
	reg [31:0] Registers [31:0];
	
	/*
	reg [31:0]	zero;	//0
	reg [31:0]	at;	//1
	reg [31:0]	v0;	//2
	reg [31:0]	v1;	//3
	reg [31:0]	a0;	//4
	reg [31:0]	a1;	//5
	reg [31:0]	a2;	//6
	reg [31:0]	a3;	//7
	reg [31:0]	t0;	//8
	reg [31:0]	t1;	//9
	reg [31:0]	t2;	//10
	reg [31:0]	t3;	//11
	reg [31:0]	t4;	//12
	reg [31:0]	t5;	//13
	reg [31:0]	t6;	//14
	reg [31:0]	t7;	//15
	reg [31:0]	s0;	//16
	reg [31:0]	s1;	//17
	reg [31:0]	s2;	//18
	reg [31:0]	s3;	//19
	reg [31:0]	s4;	//20
	reg [31:0]	s5;	//21
	reg [31:0]	s6;	//22
	reg [31:0]	s7;	//23
	reg [31:0]	t8;	//24
	reg [31:0]	t9;	//25
	reg [31:0]	k0;	//26
	reg [31:0]	k1;	//27
	reg [31:0]	gp;	//28
	reg [31:0]	sp;	//29
	reg [31:0]	fp;	//30
	reg [31:0]	ra;	//31

	*/
	
	initial begin
		Registers[0] <= 8'h00000000;
		Registers[1] <= 8'h00000000;
		Registers[2] <= 8'h00000000;
		Registers[3] <= 8'h00000000;
		Registers[4] <= 8'h00000000;
		Registers[5] <= 8'h00000000;
		Registers[6] <= 8'h00000000;
		Registers[7] <= 8'h00000000;
		Registers[8] <= 8'h00000001;
		Registers[9] <= 8'h00000000;
		Registers[10] <= 8'h00000000;
		Registers[11] <= 8'h00000000;
		Registers[12] <= 8'h00000000;
		Registers[13] <= 8'h00000000;
		Registers[14] <= 8'h00000000;
		Registers[15] <= 8'h00000000;
		Registers[16] <= 8'h00000000;
		Registers[17] <= 8'h00000000;
		Registers[18] <= 8'h00000000;
		Registers[19] <= 8'h00000000;
		Registers[20] <= 8'h00000000;
		Registers[21] <= 8'h00000000;
		Registers[22] <= 8'h00000000;
		Registers[23] <= 8'h00000000;
		Registers[24] <= 8'h00000000;
		Registers[25] <= 8'h00000000;
		Registers[26] <= 8'h00000000;
		Registers[27] <= 8'h00000000;
		Registers[28] <= 8'h00000000;
		Registers[29] <= 8'h00000000;
		Registers[30] <= 8'h00000000;
		Registers[31] <= 8'h00000000;
	end
	
		assign RD1 = Registers[A1];
		assign RD2 = Registers[A2];
	
	always @(negedge clk) 
	begin
	
		if(WE3==1'b1)	//Indicates need to write what's in A3
		begin
			Registers[A3] = WD3;	
		end
		
	end

endmodule
