`ifndef _EN
`define _EN

module EN(Clk, rst, in,out,enable);

	parameter width = 1;

	input [width-1:0] in ;
	input Clk,enable, rst;

	output [width-1:0] out;
	
	wire [width-1:0] in;
	wire rst;
	reg [width-1:0] out;
	

	always @(posedge Clk) 
	begin
		if(enable)
		begin
			assign out = rst ? 1'b0 : in;
		end
	end
endmodule
	

`endif	
			