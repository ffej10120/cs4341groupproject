As discussed in class, the topic of the team project this semester is to implement the datapath and control of a 32-bit MIPS processor using Verilog that can be simulated and demonstrated using ModelSIM, so please go ahead and start working on the project. 

Submission requirements:

Everyone must submit a ZIP (no RAR please) file that contains:

1) Text files of ALL Verilog modules.

2) Sample MIPS assembly language programs used to test your system and their machine codes.

3) A MS Word document that serves as documentation of your system. It must document features and limitations of the system.

4) A short video clip showing the behavior of the datapath and control of the processor when it is executing a few MIPS instructions (ideally all 3 types: R, I and J) as simulated by ModelSIM.

5) A project report in MS Word format that contains

   a. Team member names, tasks were done by you and tasks done by each of your teammates. You should document here how every teammate has contributed and especially if you have made extra contributions to the project. Our goal is to grade each team member fairly using the information documented in this sub-section.

   b. Project description (the WHAT)

   c. How the program was implemented  (the HOW) 

   d. The process the team took to tackle the assignment

   e. Challenges met during the project on how the team overcame them

   f. What you have learned doing the project.

Obviously, many items can be shared by all team members but items a. and f. are individual.

Feel free to contact me if there is any question.
