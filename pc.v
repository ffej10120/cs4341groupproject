module PC(pc, in, clk);
input [31:0] in; // Input value
input clk;

output [31:0] pc;
reg [31:0] pc;


initial begin 
	pc = 32'h00000000;
end

always @(negedge clk)
begin
	pc = in;
	end
	
endmodule

module PC_Mux(newpc, pc, extendamt, branchchk, jump, jumpAddress);
input [31:0] pc;
input [31:0] extendamt;
input branchchk, jump;
input [25:0] jumpAddress;
output [31:0] newpc;
wire [31:0] checkforbranch;


assign checkforbranch = branchchk ? (pc+extendamt):(pc+4);

assign newpc = jump ? {checkforbranch[31:28], jumpAddress} : checkforbranch;

endmodule

