`ifndef _halfAdder//So module is not added multiple times
`define _halfAdder

module halfAdder(a, b, sum, cout);

//Port definitions
input a;
input b;

output sum;
output cout;
	
//Declare port datatypes (wire for simple in/out, reg for data storage)
wire a;
wire b;
wire sum;
wire cout;

//Module code

assign {cout,sum} = a + b; //add a + b storing overflow to cout
endmodule

`endif