//-------------------------------------
//
//	MIPS Processor
//
//-------------------------------------
//	Julian McNichols
//	CS 4341.002
//
//
//-------------------------------------

`include "InstructionMemory.v"
`include "RegisterFile.v"
`include "fullAdder.v"
`include "halfAdder.v"
`include "ShiftLeft2.v"
`include "SignExtender.v"
`include "pc.v"

module MIPSMain(Clk);

	input Clk;

	wire [31:0] pc, instr, result, RS, RT, out, PCBranch, newpc;
	wire [5:0] opcode, funct;
	wire [4:0] rs, rt, rd;
	wire [15:0] addr;
	wire [31:0] srcA;
	wire [31:0] aluin2;
	reg [31:0] aluinaddr;
	reg [31:0] aluindata;
	reg currentclksig;
	reg [31:0] in;
	reg [31:0] currentAddress;
	wire [31:0] finalResult;
	wire [31:0] newPc;
	
	
	wire [4:0] writeReg;
	wire [31:0] srcB;
	wire [31:0] signImm;
	wire [31:0] aluResult;
	
	//Control Unit Wires
	wire regDst, jump, branch, memRead, memToReg, memWrite, aluSrc, regWrite;
	wire [2:0] aluControl;
	wire [25:0] jumpAddress;
	
	assign writeReg = regDst ? instr[15:11] : instr[20:16];
	assign srcB = aluSrc ? signImm : aluin2;
	assign finalResult = aluResult;
	//assign aluSrc = branch & ~aluResult[0];
	//Modules
	PC 					 ProgramCounter (pc, in, Clk);
	PC_Mux				 ProgramCounterMux (newPc, pc, PCBranch, branch, jump, jumpAddress);
	InstructionMemory	 Memory 		(instr,pc,Clk);
	control				 controlModule 	(Clk,(instr[31:26]),(instr[5:0]), regDst, jump, branch, memRead, memToReg, aluControl, memWrite, aluSrc, regWrite);
	RegisterFile		 RegisterFile	(instr[25:21], instr[20:16], writeReg, finalResult, regWrite, srcA, aluin2, Clk); 
	ALU 				 aluModule 		(aluControl, srcA, srcB, aluResult);
	SignExtender 		 extender		(instr[15:0],signImm);
	ShiftLeft2			 shiftLeft		(signImm,PCBranch);
	ShiftLeft2			 JumpShifter	(instr[25:0], jumpAddress);
	

	always @(*)
	begin
		in=newPc;
	end
	
	
endmodule
